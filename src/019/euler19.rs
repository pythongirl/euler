use chrono::prelude::*; // 0.4.9

struct DateRange {
    current: NaiveDate,
    end: NaiveDate,
}

impl DateRange {
    fn new(start: NaiveDate, end: NaiveDate) -> DateRange {
        DateRange {
            end,
            current: start,
        }
    }
}

impl Iterator for DateRange {
    type Item = NaiveDate;

    fn next(&mut self) -> Option<NaiveDate> {
        let next = self.current.succ();

        if next < self.end {
            let current = self.current;
            self.current = next;
            Some(current)
        } else {
            None
        }
    }
}

fn main() {
    println!(
        "{:?}",
        DateRange::new(
            NaiveDate::from_ymd(1901, 1, 1),
            NaiveDate::from_ymd(2000, 12, 31)
        )
        .filter(|d| d.day() == 1 && d.weekday() == Weekday::Sun)
        .count()
    );
}