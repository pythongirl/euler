use crate::utils::iter_wrapper::IterWrapper;
use crate::Problem;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 2;

    fn solve() -> i64 {
        let fibonacci_iterator = IterWrapper::new((0, 1), |&(a, b)| (Some(a), (b, a + b)));

        fibonacci_iterator
            .filter(|n| n % 2 == 0)
            .take_while(|&n| n < 4_000_000)
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 4613732);
    }
}
