use crate::Problem;
use crate::utils::primes::PrimeIterator;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 10;

    fn solve() -> i64 {
        PrimeIterator::<i64>::new()
            .take_while(|&p| p < 2_000_000)
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 142913828922);
    }
}