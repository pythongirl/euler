use crate::Problem;
use num::Rational;
use crate::utils::polynomial::Polynomial;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 28;

    fn solve() -> i64 {
        // solution code
        
        let p = Polynomial::<Rational>::x() * Rational::from_integer(2) - Rational::from_integer(1);

        0
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 0);
    }
}