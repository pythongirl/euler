#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <gmp.h>

void print(const char *msg){
	printf("%s\n", msg);
}

bool check_num(mpz_t n){
	char *s = mpz_get_str(NULL, 10, n);

	int len = strlen(s);

	int digits = 0;
	for(int i = 0; i < 9; i++){
		digits |= 1 << (s[i] - 0x30);
	}
	bool front = !(0b1111111110 & ~digits);

	digits = 0;
	for(int i = 9; i > 0; i--){
		digits |= 1 << (s[len - i] - 0x30);
	}
	bool back = !(0b1111111110 & ~digits);

	free(s);
	return front && back;
}

int main(){
	int n = 3;
	mpz_t a, b, c;

	mpz_init_set_ui(a, 1);
	mpz_init_set_ui(b, 1);
	mpz_init(c);

	while(true){
		if(n % 10000 == 0){
			printf("n: %d\n", n);
		}

		mpz_add(c, a, b);
		mpz_set(a, b);
		mpz_set(b, c);

		if(check_num(c)){
			printf("FOUND (%d):\n", n);
			mpz_out_str(stdout, 10, c);
			return 0;
		}

		n++;
	}

	return 0;
}
