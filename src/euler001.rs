use crate::Problem;

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 1;

    fn solve() -> i64 {
        (1..1000).filter(|x| x % 3 == 0 || x % 5 == 0).sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 233168);
    }
}