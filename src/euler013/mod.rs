use crate::Problem;

const DATA: &'static str = include_str!("euler13.data.txt");

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 13;

    fn solve() -> i64 {
        DATA.lines()
            .map(|l| l.parse::<num::BigInt>().unwrap())
            .sum::<num::BigInt>()
            .to_string()[0..10].parse::<i64>().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 5537376230);
    }
}