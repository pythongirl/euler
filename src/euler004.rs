use crate::Problem;

fn is_palindrome(number: i64) -> bool {
    let number_string = format!("{}", number);
    let mut reversed_number = number_string.as_bytes().to_vec();
    reversed_number.reverse();

    number_string.as_bytes() == reversed_number.as_slice()
}

pub struct Solution;

impl Problem for Solution {
    const NUMBER: usize = 4;

    fn solve() -> i64 {
        (0..1000i64)
            .flat_map(|x| (0..1000).map(move |y| (x, y)))
            .map(|(x, y)| x * y)
            .filter(|&p| is_palindrome(p))
            .max()
            .unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn check_answer() {
        assert_eq!(Solution::solve(), 906609);
    }
}
