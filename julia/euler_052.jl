### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ ca18d01e-0203-11ed-265b-b19baba9aac6
function test(x)
	all(==(sort(digits(x))), [2,3,4,5,6] .* x .|> digits .|> sort)
end

# ╔═╡ ba06ade0-6ff4-4553-b668-174b24929300
function search()
	i = 1
	while !test(i)
		i += 1
	end
	i
end

# ╔═╡ 458f3b70-fd53-4e3b-96a4-e7416ca98221
const result = search()

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═ca18d01e-0203-11ed-265b-b19baba9aac6
# ╠═ba06ade0-6ff4-4553-b668-174b24929300
# ╠═458f3b70-fd53-4e3b-96a4-e7416ca98221
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
