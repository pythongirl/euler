### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ 2b374ed2-ff02-11ec-0280-c5d40f577aec
using LinearAlgebra

# ╔═╡ a9d07e22-adb6-4e98-89f3-abc444b53202
function permutation_matrix(w, i)::Matrix{Bool}  
	function swap_matrix(w, i)
		m = Matrix{Bool}(I, w,w)
		m[i,i] = 0
		m[i,1] = 1
		m[1,1] = 0
		m[1,i] = 1
		m
	end
	
	extend_matrix(m, h,w) =
		[
			Matrix{Bool}(I,h-size(m, 1),w-size(m,2)) falses(h-size(m,1),size(m, 2));
			falses(size(m,1), w-size(m,2)) m
		]
	
	if w > 1
		swap_matrix(w, (i % w)+1) * extend_matrix(permutation_matrix(w-1, i÷w), w, w)
	else
		swap_matrix(1, 1)
	end
end

# ╔═╡ 877b030b-21ee-4aea-bcff-f5508c1304cf
try_product(ds, split) =
	evalpoly(10, @view ds[1:split]) * evalpoly(10, @view ds[split+1:5]) == evalpoly(10, @view ds[6:9])

# ╔═╡ 42f8c076-3539-4ece-807e-b105f7caeeab
function try_products()
	s = Set()
	for i=0:(factorial(9)-1)
		perm = permutation_matrix(9,i)
		digits = perm * (1:9)

		for t=1:2
			if try_product(digits, t)

				# a = evalpoly(10, @view(digits[1:t]))
				# b = evalpoly(10, @view(digits[t+1:5]))
				c = evalpoly(10, @view(digits[6:9]))
				# println("Found $(a) × $(b) = $(c)")
				
				push!(s, c)

				
			end
		end
	end
	s
end

# ╔═╡ 1794b0b2-04aa-4a23-a693-a221fa6f67bd
const result = sum(try_products())

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
LinearAlgebra = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═2b374ed2-ff02-11ec-0280-c5d40f577aec
# ╠═a9d07e22-adb6-4e98-89f3-abc444b53202
# ╠═877b030b-21ee-4aea-bcff-f5508c1304cf
# ╠═42f8c076-3539-4ece-807e-b105f7caeeab
# ╠═1794b0b2-04aa-4a23-a693-a221fa6f67bd
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
