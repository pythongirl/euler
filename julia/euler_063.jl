### A Pluto.jl notebook ###
# v0.19.8

using Markdown
using InteractiveUtils

# ╔═╡ e72cd419-65be-4ba8-955b-2fd2511dbd1e
# n <= (log10(i) * n + 1) < n+1
# 1 <= (log10(i) + 1/n) < (1 + 1/n)

# 1 <= log10(i) + 1/n
# 1 - log10(i) <= 1/n
# (1 - log10(i)) * n <= 1
# n <= 1 / (1 - log10(i))
# the critical inequality

# (log10(i) + 1/n) < (1 + 1/n)
# log10(i) < 1
# just means that i must be < 10

# ╔═╡ 5c1d9158-2549-4b1c-a0e4-92eba1aeea03
const result = 1:9 .|> (i -> 1 / (1 - log10(i))) .|> floor |> sum

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.3"
manifest_format = "2.0"

[deps]
"""

# ╔═╡ Cell order:
# ╠═e72cd419-65be-4ba8-955b-2fd2511dbd1e
# ╠═5c1d9158-2549-4b1c-a0e4-92eba1aeea03
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
