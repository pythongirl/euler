# Euler

My Solutions to Project Euler problems.

Don't look at this if you don't want the answers spoiled for you!

## Todos
Convert project to a rust crate with one module for each day

Days that need rust solutions:

- redo problem 19

- TODO: convert problem 21
- TODO: convert problem 22
- TODO: convert problem 25
- TODO: convert problem 28
- TODO: convert problem 29
- TODO: convert problem 30
- TODO: convert problem 34
- TODO: convert problem 36
- TODO: convert problem 39
- TODO: convert problem 42
- TODO: convert problem 48
- TODO: convert problem 67
- TODO: convert problem 104
