#include <stdio.h>
#include <stdbool.h>
#include <string.h>

bool isPalindrome(int number){
  char buf[16];
  sprintf(buf, "%d", number);
  
  int l;
  int h = strlen(buf) - 1;
  
  while (h > l)
  {
    if (buf[l++] != buf[h--])
    {
      return false;
    }
  }
  return true;
}


int main(){
  int max_number, max_x, max_y;
  
  for(int x = 1; x < 1000; x++){
    for(int y = 1; y < 1000; y++){
      int number = x * y;
      if(isPalindrome(number)){
        if(number > max_number){
          max_number = number;
          max_x = x;
          max_y = y;
        }
      }
    }
  }
  
  printf("%d * %d = %d", max_x, max_y, max_number);
  
  return 0;
}