use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::cmp::max;

fn main() {
    let file = File::open("triangle.txt").expect("unable to open file");
    let reader = BufReader::new(file);

    let triangle: Vec<Vec<u32>> = reader.lines().map( |line| {
        let l = line.unwrap();

        l.split(' ').map(|s| {
            s.parse::<u32>().expect("non-integer in file")
        }).collect()
    }).collect();
    
    let mut solution_row: Vec<u32> = vec![];
    let mut prev_solution_row: Vec<u32> = triangle.last().cloned().unwrap();

    for n in (0..triangle.len()-1).rev() {
        solution_row = triangle[n].iter().enumerate().map(|(i, v)| {
            v + max(prev_solution_row[i], prev_solution_row[i + 1])
        }).collect();

        prev_solution_row = solution_row.clone();
    }

    println!("Final: {}", solution_row[0]);


}
