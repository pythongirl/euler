#!/usr/bin/env python3

s = set()

for a in range(2, 101):
    for b in range(2, 101):
        s = s | set([a ** b])

print(f"{len(s)} terms")
#print(s)
