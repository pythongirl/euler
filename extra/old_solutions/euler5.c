#include <stdio.h>
#include <stdbool.h>

bool isDivisible(int number){
  for(int d = 1; d <= 20; d++){
    if(number % d != 0){
      return false;
    }
  }
  return true;
}

int main(){
  int i = 1;
  
  while(!isDivisible(++i));
  
  printf("Found it! Number: %d\n", i);
  
  return 0;
}