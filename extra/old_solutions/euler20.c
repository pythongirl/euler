#include <stdio.h>
#include <gmp.h>

int factorial(mpz_t output, mpz_t input){
  mpz_t i;

  mpz_init_set(i, input);

  mpz_set_ui(output, 1);

  for(; mpz_get_ui(i) >= 2; mpz_sub_ui(i, i, 1)){
    mpz_mul(output, output, i);
  }

  mpz_clear(i);

  return 0;
}

int sum_of_digits(mpz_t output, mpz_t input){
  mpz_t mod_result, input_copy;
  mpz_init(mod_result);
  mpz_init_set(input_copy, input);

  while(mpz_cmp_ui(input_copy, 1) >= 1){
    mpz_mod_ui(mod_result, input_copy, 10);
    mpz_add(output, output, mod_result);

    mpz_fdiv_q_ui(input_copy, input_copy, 10);
  }

  mpz_clear(mod_result);
  mpz_clear(input_copy);

  return 0;
}

int main(){
  mpz_t n, sum;

  mpz_init_set_ui(n, 100);
  mpz_init_set_ui(sum, 0);

  factorial(n, n);

  sum_of_digits(sum, n);

  mpz_out_str(stdout, 10, sum);
  printf("\n");

  mpz_clear(n);
  mpz_clear(sum);

  return 0;
}